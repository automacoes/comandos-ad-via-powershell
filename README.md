# Comandos AD via Powershell

#### Vários grupos para um usuário
```
Add-ADPrincipalGroupMembership claudio.silva -MemberOf "Acesso Wireless","Acesso Wireless VIPs"
```
Remover vários grupos para um usuário
```
Remove-ADPrincipalGroupMembership -Identity claudio.silva -MemberOf 'SQL Administrators', 'WiFi Administrators' -Confirm:$False
```
